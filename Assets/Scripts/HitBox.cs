﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class HitBox : MonoBehaviour
{
    public event Action<Collider2D> OnHitEvent;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("OnTrigger Enter");
        OnHitEvent?.Invoke(other);
    }
}
