﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private float walkForce;
    [SerializeField] private float attackForce;
    [SerializeField] private float swordPoGoForce;
    [SerializeField] private float attackDuration;
    [SerializeField] private float attackCooldownDuration;
    [SerializeField] private GameObject spriteRoot;

    [SerializeField] private HitBox swordHitBox;
    
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;

    [SerializeField] private int attackFrameBufferSize = 5;
    private Queue<bool> _attackInputBuffer;
    private bool _attackInputThisFrame;
    private bool _attackOnCooldown;
    private bool _isAttacking;
    private Coroutine _attackCoroutine;

    private Vector2 _directionalInput;

    private bool _hasDirectionInput;

    private bool _isFacingRight;

    private static readonly int AttackTriggerId = Animator.StringToHash("Attack");
    
    private float _startGravity;
    private float _startDrag;
    private static readonly int XInputId = Animator.StringToHash("XInput");
    private static readonly int YInput = Animator.StringToHash("YInput");
    private static readonly int AttackXDirectionId = Animator.StringToHash("AttackXDirection");
    private static readonly int AttackYDirectionId = Animator.StringToHash("AttackYDirection");

    [SerializeField] private float groundCheckDistance;
    private bool _isGrounded = true;
    private bool _canAirDash = false;

    private void Awake()
    {
        _directionalInput = Vector2.zero;
        _attackInputBuffer = new Queue<bool>(attackFrameBufferSize);
        _animator = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _startDrag = _rigidbody2D.drag;
        _startGravity = _rigidbody2D.gravityScale;
        
    }

    private void Start()
    {
        _isFacingRight = true;
        swordHitBox.OnHitEvent += OnSwordHit;
    }

    private void Update()
    {
        UpdateInputState();

        Vector3 currentPosition = transform.position;

        Collider2D[] colliders = new Collider2D[10];
        int size = Physics2D.OverlapCircleNonAlloc(new Vector2(currentPosition.x, currentPosition.y), groundCheckDistance, colliders);

        _isGrounded = false;
        for (int i = 0; i < size; i++)
        {
            Collider2D hit = colliders[i];
            if (!hit.CompareTag("Player"))
            {
                Debug.Log("Grounded");
                _isGrounded = true;
                break;
            }
            
        }
        

        if (_hasDirectionInput && !_isAttacking)
        {
            _rigidbody2D.AddForce(walkForce * _directionalInput.x * Vector2.right);
        }

        if (CanAttack())
        {
//            int xInput = Mathf.RoundToInt(_directionalInput.x);
//            int yInput = Mathf.RoundToInt(_directionalInput.y);
//            if (Mathf.Abs(xInput) == 1 && yInput == 0)
//            {
//                Debug.Log("Forward Slash");
//            } else if (Mathf.Abs(xInput) == 1 && yInput == 1)
//            {
//                Debug.Log("Diagonal Slash");
//            } else if (yInput == -1)
//            {
//                Debug.Log("Down Slash");
//            } else if (yInput == 1)
//            {
//                _attackCoroutine = StartCoroutine(UpwardSlash());
//            }
            _attackCoroutine = StartCoroutine(Attack());
        }

        UpdateSpriteDirection();
    }

    private void UpdateSpriteDirection()
    {
        int xInput = Mathf.RoundToInt(_directionalInput.x);
//        _animator.SetFloat(XInputId, xInput);
        if (xInput != 0)
        {
            spriteRoot.transform.localScale = new Vector3( Mathf.Sign(xInput), 1, 1);
        }
    }

    private bool CanAttack()
    {
        bool isAttacking = _attackCoroutine != null;
        bool gotAttackSignal = false;
        foreach (bool bufferedAttackSignal in _attackInputBuffer)
        {
            if (!bufferedAttackSignal) continue;
            gotAttackSignal = true;
            break;
        }
        return !isAttacking && gotAttackSignal;
    }
    
    private void UpdateInputState()
    {
        _attackInputThisFrame = Input.GetKeyDown(KeyCode.Space);
        _attackInputBuffer.Enqueue(_attackInputThisFrame);

        if (_attackInputBuffer.Count > attackFrameBufferSize)
        {
            _attackInputBuffer.Dequeue();
        }


        float xInput = Input.GetAxisRaw("Horizontal");
        float yInput = Input.GetAxisRaw("Vertical");

        _hasDirectionInput = Mathf.Abs(xInput) + Mathf.Abs(yInput) >= Mathf.Epsilon;

        if (Math.Abs(xInput) > Mathf.Epsilon)
        {
            _isFacingRight = xInput > 0;
        }

        _directionalInput = new Vector2(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical")
        );
    }

    private void DebugLogAttackBuffer()
    {
        Debug.Log("[" + string.Join(",", _attackInputBuffer.ToArray()) + "]");
    }

    private IEnumerator Attack()
    {
        Debug.Log("Slash");
        _isAttacking = true;
        
//        _rigidbody2D.velocity = Vector2.zero;
        if (_isGrounded || _canAirDash)
        {
            _rigidbody2D.drag = 0f;
            _rigidbody2D.gravityScale = 0f;
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 0);
            _rigidbody2D.AddForce(_directionalInput.normalized * attackForce, ForceMode2D.Impulse);
            _canAirDash = false;
        }
        _animator.SetFloat(AttackXDirectionId, _directionalInput.x);
        _animator.SetFloat(AttackYDirectionId, _directionalInput.y);
        _animator.SetTrigger(AttackTriggerId);
        yield return new WaitForSeconds(attackDuration);
        _isAttacking = false;
        _rigidbody2D.gravityScale = _startGravity;
        _rigidbody2D.drag = _startDrag;
        yield return new WaitForSeconds(attackCooldownDuration);
        _attackCoroutine = null;
    }

    private void OnSwordHit(Collider2D other)
    {
        Debug.Log(other.name);
        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 0);
        _rigidbody2D.AddForce(Vector2.up * swordPoGoForce, ForceMode2D.Impulse);
        _canAirDash = true;
    }

    private IEnumerator UpwardSlash()
    {
        Debug.Log("Upward Slash");
        yield break;
    }
}